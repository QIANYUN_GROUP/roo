<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>


<div style="padding: 5px;overflow: hidden;">
	<form id="detailForm">
	<table class="details_table" style="margin: 0 auto;" margin-top:12px;" cellspacing="0"  width="100%">
         <tr>
				<th width="100%" height="30px" align="center" colspan="4"
					style="background-color: #f5f5f5; border-radius: 6px 6px 0 0;">用户详细</th>
		 </tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户编号：</td>
					<td colspan="3"><input type="text" name="userId" disabled="disabled"  style="border:0px;"  />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户姓名：</td>
					<td><input type="text" name="userName" disabled="disabled" style="border:0px;"  />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;机&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;构：</td>
					<td><input type="text" name="instName" disabled="disabled" class="easyui-combotree" 
					    data-options="url:'um/inst/getInstCombotree.action',hasDownArrow:false" style="border:0px;"/>
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;所属部门：</td>
					<td><input type="text" name="dept" disabled="disabled" class="easyui-combobox"
					    data-options="url:'pm/dict/dictCombobox.action?dictTypeId=100001',hasDownArrow:false" style="border:0px;"  />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户角色：</td>
					<td><input type="text" name="roleName" disabled="disabled" style="border:0px;"  />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户性别：</td>
					<td><input type="text" name="userSex" disabled="disabled" class="easyui-combobox" 
					    data-options="url:'pm/dict/dictCombobox.action?dictTypeId=100002',hasDownArrow:false" style="border:0px;" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户状态：</td>
					<td><input type="text" name="userState" disabled="disabled" class="easyui-combobox" 
					    data-options="url:'pm/dict/dictCombobox.action?dictTypeId=100003',hasDownArrow:false" style="border:0px;"  />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;电子邮箱：</td>
					<td><input type="text" name="email" disabled="disabled" style="border:0px;" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;联系方式：</td>
					<td><input type="text" name="contactWay" disabled="disabled" style="border:0px;"  />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;创建日期：</td>
					<td><input type="text" name="creationDate" class="Wdate"
						onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						disabled="disabled" style="border:0px;" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;最后更新：</td>
					<td><input type="text" name="lastUpDate" class="Wdate"
						onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						disabled="disabled" style="border:0px;" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;联系地址：</td>
					<td colspan="3"><input type="text" name="address"
						disabled="disabled" style="width: 443px;" style="border:0px;" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
					<td colspan="3"><input name="remark" disabled="disabled"
							style="width: 443px;resize: none;" />
					</td>
				</tr>
			</table>
	</form>
</div>