<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<html>
<head>
<title>用户管理</title>
<script type="text/javascript" charset="UTF-8">
	$(function() {
		//列表
		datagraid2({ 
			singleSelect: true, //单选
			url : 'main/datagrid',
			detailsutl : 'main/datagrid',
			width : 600,
			height : 365,
			columns : [[
			    {checkbox : true},
				{field : 'id', title : 'id', width : 10, sortable : true}, //排序  
				{field : 'memberId', title : 'memberId', width : 11, sortable : true},
				{field : 'operator', title : 'operator', width : 11, sortable : true},
				{field : 'name', title : 'name', width : 10, sortable : true},
				{field : 'bidInfo', title : 'bidInfo', width : 10, sortable : true},
				{field : 'umemo', title : 'memo', width : 10}
	            
			]]
		});
		//查询
		$('#btn_qry').click(function() {
			crud({
				title : '用户信息查询',
				width : 300,
				height : 220
			}).search();
		});
		
		var curUserId = $('#userId').val();
		//新增
		$('#btn_add').click(function() {
			crud({
				width : 600,
				height : 265,
				title : '用户信息新增',
				url : 'main/initAdd',
				suburl : 'main/add'
			}).add();
		});
		//修改
		$('#btn_mod').click(function() {
			var row = $('#tableList').datagrid('getSelected');
			if ($('#tableList').datagrid('getSelections').length != 1) {
				$.messager.alert('提示', '请选择一行！');
				return;
			}
			if (row.userId == curUserId) {
				$.messager.alert('提示', '用户不能操作自己！');
	        	return;
			}
			crud({
				width : 600,
				height : 300,
				title : '用户信息修改',
				url : 'um/user/initMod.action',
				suburl : 'um/user/mod.action'
			}).update();
		});
		//删除
		$('#btn_del').click(function() {
			crud({
				suburl : 'main/del.action',
				delparm : ['id']
			}).del();
		});
		
	});
</script>
</head>
<body>
    <!--  查询div -->
    <div id="qryDiv" style="padding: 5px;overflow: hidden;display: none;">
		<form id="qryForm">
			<input type="hidden" type="text" name="remark" value="123"/>
			<table class="details_table" style="margin: 0 auto;" width="100%" cellspacing="0" >
                    <tr>
				        <th width="100%" height="30px" align="center" colspan="2"
					        style="background-color: #f5f5f5; border-radius: 6px 6px 0 0;">用户查询</th>
		            </tr> 
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;用户编号：</td>
						<td><input type="text" name="userId" maxlength="10" />
						</td>
					</tr>
					
				</table> 
		</form>
	</div>
	
	<!-- 功能按钮 -->
	<div id="btn">
	 <a id="btn_qry" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>&nbsp;&nbsp;
	 <a id="btn_add" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>&nbsp;&nbsp;
	 <a id="btn_mod" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改</a>&nbsp;&nbsp; 
	 <a id="btn_del" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>&nbsp;&nbsp; 
	   
	</div>
	
	<!-- 列表信息 -->
	<table id="tableList"></table>
	
	<input type="hidden" id="userId" value="${sessionScope['USER_KEY'].userId}" />
</body>
</html>