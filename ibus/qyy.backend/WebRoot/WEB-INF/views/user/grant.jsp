<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div style="padding: 5px;overflow: hidden;">
	<form id="grantForm">
			<input type="hidden" name="userId" value="${id}" />
			<table class="details_table" style="margin: 0 auto;" margin-top:12px;" cellspacing="0"  width="100%">
               <tr>
				  <th width="100%" height="30px" align="center" colspan="4"
					style="background-color: #f5f5f5; border-radius: 6px 6px 0 0;">角色授予</th>
		       </tr> 
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;角色列表：</td>
					<td><select class="easyui-combobox" name="roleId"
						data-options="required:true" style="width:155px;">
							<option value=""></option>
							<c:forEach items="${roleList}" var="roleList">
								<option value="${roleList.roleId}"
									<c:if test="${roleList.roleId == existRole.roleId}">selected="selected"</c:if>>${roleList.roleName}</option>
							</c:forEach>
					</select></td>
				</tr>
			</table> 
	</form>
</div>