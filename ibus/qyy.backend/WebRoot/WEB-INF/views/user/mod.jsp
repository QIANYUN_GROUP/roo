<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div style="padding: 5px;overflow: hidden;">
	<form id="modForm">
		<table class="details_table" style="margin: 0 auto;" width="100%" cellspacing="0" >
         <tr>
				<th width="100%" height="30px" align="center" colspan="4"
					style="background-color: #f5f5f5; border-radius: 6px 6px 0 0;">用户修改</th>
		 </tr> 
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户编号：</td>
					<td><input type="text" name="userId" class="togray" readonly="readonly" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户姓名：</td>
					<td><input type="text" name="userName" class="easyui-validatebox"
						data-options="required:true"
						maxlength="20" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;机&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;构：</td>
					<td><input type="text" name="instId" class="easyui-combotree" 
					     data-options="url:'um/inst/getInstCombotree.action',hasDownArrow:true" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;所属部门：</td>
					<td><input type="text" name="dept" class="easyui-combobox" 
					    data-options="url:'pm/dict/dictCombobox.action?dictTypeId=100001',hasDownArrow:true" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;用户性别：</td>
					<td><input type="text" name="userSex" class="easyui-combobox" 
					    data-options="url:'pm/dict/dictCombobox.action?dictTypeId=100002',required:true" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;电子邮箱：</td>
					<td><input type="text" name="email" class="easyui-validatebox"
						data-options="validType:'email'" maxlength="30" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;联系方式：</td>
					<td><input type="text" name="contactWay" maxlength="30" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;联系地址：</td>
					<td><input type="text" name="address"
						maxlength="100" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
					<td colspan="3"><input name="remark" maxlength="125" style="width: 425px;" />
					</td>
				</tr>
			</table>
	</form>
</div>