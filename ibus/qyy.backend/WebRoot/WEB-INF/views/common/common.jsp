<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	response.setHeader("Pragma","no-cache");
	response.setHeader("Cache-Control","no-cache");
	response.setDateHeader("Expires", 0);
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="shortcut icon" href="<%=basePath%>img/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>css/main.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>js/jquery-easyui-1.3.3/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>js/jquery-easyui-1.3.3/themes/gray/easyui.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>js/jquery-easyui-portal/portal.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>js/uploadify/uploadify.css" />
<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/jquery-easyui-1.3.3/jquery.easyui.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/jquery-easyui-portal/jquery.portal.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/uploadify/jquery.uploadify.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/ajaxfileupload.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/My97DatePicker/WdatePicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/util.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/common.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/easyuiExtend.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/datagrid-detailview.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/jquery.edatagrid.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath%>js/extend/common.js" charset="UTF-8"></script>
<base href="<%=basePath%>">
