<%-- <%@page import="org.apache.poi.ss.formula.atp.WorkdayCalculator"%> --%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/common.jsp"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
long sday = System.currentTimeMillis();
Date date = new Date(sday);
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
String workdate = sdf.format(date); 
%>
<html>
<head>
<title>金融p2p管理系统</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<style type="text/css">

.layout-panel-west .panel-header{background:url('img/leftmainbggxl.jpg') repeat-x;height:24px;}
.layout-panel-west .panel-header .panel-title{height:24px;line-height:24px;font-size:12px;color:#333333;padding-left:20px;}
.layout-panel-west .accordion-header{background:url('img/ssww.jpg') repeat-x;height:24px}
.layout-panel-west .accordion-header .panel-title{height:24px;line-height:24px;font-size:12px;padding-left:25px; margin-left:20px; }
.layout-panel-west .accordion-header .panel-tool{display:none;}

.layout-panel-west .panel-header,.layout-panel-west .panel-body{border:0;background-color:#f4f4f4;}

.layout-panel-west .tree{padding:5px 0}
.layout-panel-west .tree li{padding-left:15px}
.layout-panel-west .tree .tree-indent{display:none}
.layout-panel-west .tree .tree-file{height:24px;line-height:24px;background-image:url('img/tree_iconss.png');background-position:0 5px;}
.layout-panel-west .tree .tree-node{height:24px;line-height:24px;display:inline-block;border:1px solid #f4f4f4;padding:0 7px;border-radius:5px;}
.layout-panel-west .tree .tree-node-hover,.layout-panel-west .tree .tree-node-selected{color:#f04e30;background:none;border-color:#999;background:#f7f7f7;}
.layout-panel-west .tree .tree-title{height:24px;line-height:24px;color:#333333;}

.btn_exit { background:url('img/button/exit.png');cursor: pointer;margin:6px 5px 5px 2px;height:25px;width:42px;float:left; }
.btn_exit:hover { background-position: 0 25px;}

</style>
<script type="text/javascript" charset="UTF-8">
	var centerTabs;
	var centerTabs2;
	var tabsMenu;
	$(function() {
		
		var errMsg = $('#errMsg').val();
		var pwdExpireMsg = $('#pwdExpireMsg').val();
		if (errMsg != null && errMsg.trim() != '') {
			$.messager.alert('错误', errMsg, 'error', function() {
				window.location.href = '<%=basePath%>main/logout.action'; 
			});
		}
		if (pwdExpireMsg != null && pwdExpireMsg.trim() != '') {
			$.messager.show({title : '提示', msg : pwdExpireMsg, timeout : 10000});
		}
		
		//菜单树
		$('ul[id^=subMenu]').each(function() {
			var menuid = $(this).attr('id').split('_')[1];
			$(this).tree({
				url : 'main/subMenus.action?menuId=' + menuid,
				onClick : function(node) { 
					if (node.attributes.action != '') {
					    Open(node.text, node.attributes.action);
					}
				},
				onSelect:function(node){  
				   if(node.state == 'open'){
						$(this).tree('collapse',node.target);  
					}else{
						$(this).tree('expand',node.target);  
					}
	      	 	}	  
			});
		});
		
		
		function Open(text, url) {
	        //在右边center区域打开菜单，新增tab
	        var content = createFrame(url);
	        var a = $('#centerTabs').tabs('getSelected');
	        if ($('#centerTabs').tabs('exists', text)) {
	        	$('#centerTabs').tabs('close', text);
	        }
	        $('#centerTabs').tabs('add', {
                title : text,
                closable : true,
                content : content
            });
		}
		
		function createFrame(url) {
			var s = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:95%;"></iframe>';
			return s;
		}
		
		//tab事件
		tabsMenu = $('#tabsMenu').menu({
			onClick : function(item) {
				var curTabTitle = $(this).data('tabTitle');
				var type = $(item.target).attr('id');
				if (type === 'refresh') {
					refreshTab(curTabTitle);
					return;
				}
				if (type === 'close') {
					var t = centerTabs.tabs('getTab', curTabTitle);
					if (t.panel('options').closable) {
						centerTabs.tabs('close', curTabTitle);
					}
					return;
				}
				var allTabs = centerTabs.tabs('tabs');
				var closeTabsTitle = [];
				$.each(allTabs, function() {
					var opt = $(this).panel('options');
					if (opt.closable && opt.title != curTabTitle && type === 'closeOther') {
						closeTabsTitle.push(opt.title);
					} else if (opt.closable && type === 'closeAll') {
						closeTabsTitle.push(opt.title);
					}
				});
				for ( var i = 0; i < closeTabsTitle.length; i++) {
					centerTabs.tabs('close', closeTabsTitle[i]);
				}
			}
		});
		
		//绑定tabs的右键菜单
		centerTabs = $('#centerTabs').tabs({
			fit : true,
			border : false,
			onContextMenu : function(e, title) {
				e.preventDefault();
				tabsMenu.menu('show', {
					left : e.pageX,
					top : e.pageY
				}).data('tabTitle', title);
			},
			//tabs选择敞口是刷新页面 
			onSelect:function(title){  
		    	if(title == "人民币金敞口监控" || title == "人民币金远期敞口监控" || title == "美元金敞口监控" || title == "租借敞口监控" || title == "品牌金敞口监控" || title == "总敞口监控"){
		    		refreshTab(title);
		    	}
		    },
		});
		
		//刷新tab
		function refreshTab(title) {
			var tab = centerTabs.tabs('getTab', title);
			centerTabs.tabs('update', {
				tab : tab,
				options : tab.panel('options')
			});
		}
		
		//个人信息
		$('#btn_person').click(function() {
			var d = $('<div/>').dialog({
				href : 'main/initModPersonInfo.action',
				title : '个人信息修改', width :400, height : 295, cache : false, closable : true, modal : true,
				buttons : [{
					text : '修改密码',
					handler : function() {
						var pd = $('<div/>').dialog({
							href : 'main/initModPersonPwd.action',
							title : '个人密码修改', width : 300, height : 205, cache : false, closable : true, modal : true, 
							buttons : [{
								text : '确定',
								handler : function() {
									var pf = pd.find('form');
									var pv = pf.form('validate');
									if (pv) {
										$.post('main/modPersonPwd.action', $.serializeObject(pf), function(msg) {
											if (msg.toString().indexOf('error') != -1) {
												$.messager.show({title : '提示', msg : msg.replace(/.*error:/, '')});
											} else {
												$.messager.show({title : '提示', msg : '操作成功'});
												pd.dialog('close');
											}
										});
									}
								}
							}, {
								text : '取消',
								handler : function() {
									pd.dialog('destroy');
								}
							}],
							onClose : function() {
								pd.dialog('destroy');
							}
						});
					}
				}, {
					text : '确定',
					handler : function() {
						var f = d.find('form');
						var v = f.form('validate');
						if (v) {
							$.post('main/modPersonInfo.action', $.serializeObject(f), function(msg) {
								if (msg.toString().indexOf('error') != -1) {
									$.messager.show({title : '提示', msg : msg.replace(/.*error:/, '')});
								} else {
									$.messager.show({title : '提示', msg : '操作成功'});
									d.dialog('close');
								}
							});
						}
					}
				}
// 				, {
// 					text : '重置',
// 					handler : function() {
// 						$('#personForm').form('reset');
// 					}
// 				}
				, {
					text : '取消',
					handler : function() {
						d.dialog('destroy');
					}
				}],
				onClose : function() {
					d.dialog('destroy');
				}
			});
		});
		
		//退出
		$('#btn_exit').click(function() {
			$.messager.confirm('提示','确定要退出吗？', function(r) {   
			    if (r) {   
			    	window.location.href = '<%=basePath%>main/logout.action'; 
			    }   
			});  
		});
		
		$('#alertInfo').click(function(){
			//$.messager.alert('消息','报警信息');
			var d = $('<div/>').dialog({
				href : 'jsp/main/alertinfo.jsp',
				title : '报警信息', width : 600, height : 350, 
				cache : false, 
				closable : true, 
				modal : true,
				onClose : function() {
					d.dialog('destroy');
				}
			});
		}); 
	});
</script>
</head>
<body class="easyui-layout" >
    <div data-options="region:'north',border:false" style=" height: 70px; background:url('img/headbggxl.jpg') repeat-x">
    	<img src="img/logogxl.jpg" style="float:left;" />
		<div style="background:url('img/rightgxl.jpg') no-repeat right; height:70px; width:550px;float:right; ">
			<div style="margin-left:240px;margin-top:17px;float:left;background:url('img/button/btngxl.png') no-repeat;width:300px;">
				<div id="btn_person" alt="个人信息"  style="background:url('img/button/people.png'); cursor: pointer;margin:6px 2px 5px 5px;height:25px;width:223px;float:left;display:block;">
					<span style="line-height:24px;color:#fff; float:left;height:24px;width:80px;margin-left:45px; text-align:top; ">${sessionScope['USER_KEY'].userName }</span>
        	 		<span style="line-height:24px;color:#fff; float:left;height:25px;width:80px;margin-left:10px; text-align:top; ">${sessionScope['USER_KEY'].instName }</span></div>
				<div class="btn_exit" id="btn_exit" alt="退出"></div>
			</div>
		</div>
    </div>
    
    <!-- west -->
	<div data-options="region:'west',title:'菜单导航',split:false" style="width:200px;">
		<div class="easyui-accordion" data-options="fit:true,border:false,animate:false">
			<c:forEach items="${firstMenus}" var="fmenu">
			  <div title="${fmenu.name }" id="meau" >
				<ul id="subMenu_${fmenu.code}"></ul>
			  </div>
		   </c:forEach>
		</div>
	</div>

    <!-- center -->
	<div data-options="region:'center'">
    	<div id="centerTabs" data-options="border:false">
          <div id="home" title="首页" data-options="iconCls:'icon-home',border:false,href:'main/center.action'" style="overflow: hidden;">
          </div>
        </div>
    </div> 
    
    <!-- south -->
    <div id="ceshi" data-options="region:'south',border:false" style="height: 27px;line-height:27px; background-color:#ffd338; padding:0 10px;">
    	<div style="float:left;color:#9c6321;">
    	<img src="img/icon_l_01.png" style="vertical-align:middle;"> 版本：v2.0
        &nbsp;&nbsp;&nbsp;
       <img src="img/icon_l_02.png" style="vertical-align:middle"> <a id="alertInfo" href="javascript:void(0);" style="color:#9c6321;text-decoration: none;">报警信息</a></div>
       <div align="center">
		<a href="javascript:void(0);" target="_blank" style="color:#9c6321;text-decoration: none;">微贷网</a>
		<div style="float:right;color:#9c6321">
			<span>交易状态：${sessionScope['PM_BUSIPARAM'].value1 }</span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span>当前账务日期：${sessionScope['PM_BATCH'].batchdate }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下一工作日：${sessionScope['PM_BATCH'].nextDate }</span>
			
		</div>
  	</div>
    </div>  
    
    <!-- tab右击菜单 -->
    <div id="tabsMenu" style="width: 120px;display: none;">
	   <div id="refresh" data-options="iconCls:'icon-reload'">刷新</div>
	   <div class="menu-sep"></div>
	   <div id="close" data-options="iconCls:'icon-remove'">关闭</div>
	   <div id="closeOther" data-options="iconCls:'icon-no'">关闭其他</div>
	   <div id="closeAll" data-options="iconCls:'icon-cancel'">关闭所有</div>
    </div>
    <input type="hidden" id="errMsg" value="${user.errMsg}" /> 
    <input type="hidden" id="pwdExpireMsg" value="${pwdExpireMsg}" /> 

<script>
	$(function(){
		$('.layout-panel-west .panel-title').each(function(i){
			$(this).css({'background':'url("img/ll_icon_0'+(i+1)+'.png") 0 center no-repeat'});
		});
	});
</script>
</body>
</html>
